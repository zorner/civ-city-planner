package com.example.civ.city.planner.ui.view;

import javax.swing.*;

public abstract class MainView {
    private JPanel mainPanel;
    private JComboBox userComboBox;
    private JComboBox mapComboBox;
    private JButton newMapButton;
    private JLabel userLabel;
    private JLabel mapLabel;
    private JTabbedPane tabbedPane1;
    private JButton viewEntryButton;

    protected MainView() {
        //"event" is definitely not used anywhere, in case you were trying to figure that out.
        newMapButton.addActionListener(event -> newMapButtonClicked());
        viewEntryButton.addActionListener(event -> viewEntryButtonClicked());
    }

    protected abstract void newMapButtonClicked();

    protected abstract void viewEntryButtonClicked();

    public JPanel getMainPanel() {
        return mainPanel;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
