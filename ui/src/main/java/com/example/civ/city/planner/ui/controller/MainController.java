package com.example.civ.city.planner.ui.controller;

import com.example.civ.city.planner.ui.view.MainView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.swing.*;

@Controller
public class MainController extends JFrame {
    private final MainView view;

    @Autowired
    public MainController() {
        super("Civilization City Planner");

        this.view = new MainView() {
            @Override
            protected void newMapButtonClicked() {
                doNewMap();
            }

            @Override
            protected void viewEntryButtonClicked() {
                doViewEntry();
            }
        };

        this.setContentPane(view.getMainPanel());
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.pack();
    }

    private void doNewMap() {
    }

    private void doViewEntry() {
    }
}
