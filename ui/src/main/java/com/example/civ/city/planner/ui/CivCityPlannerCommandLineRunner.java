package com.example.civ.city.planner.ui;

import com.example.civ.city.planner.ui.controller.MainController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.awt.*;

@Component
public class CivCityPlannerCommandLineRunner implements CommandLineRunner {
    private final MainController controller;

    @Autowired
    public CivCityPlannerCommandLineRunner(MainController controller) {
        this.controller = controller;
    }

    @Override
    public void run(String... args) {
        //This boots up the GUI.
        EventQueue.invokeLater(() -> controller.setVisible(true));
    }
}
