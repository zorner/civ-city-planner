package com.example.civ.city.planner.ui;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class CivCityPlannerApplication {

    public static void main(String[] args) {

        new SpringApplicationBuilder(CivCityPlannerApplication.class).
                headless(false)
                .web(WebApplicationType.NONE)
                .run(args);
    }
}
